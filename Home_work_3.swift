import Foundation

func printArray (array: [Int], sizeOfMatrix: Int = 3) -> ()  {
var index = 0
    for i in 0..<sizeOfMatrix {
        print ("")
        for _ in i..<sizeOfMatrix {
            print (array[index], terminator:" ")
            index += 1
        }
    }
}

func getRndNumbersArray (sizeOfMatrix : Int = 3) -> [Int] {
    let lowBound = 10
    let upperBound = 99
    var myArray = [Int] ()
    var rnd = lowBound
    for i in 1...sizeOfMatrix {
        for _ in 0...sizeOfMatrix-i {
            let k = Int(NSDate().timeIntervalSinceReferenceDate * 1000000)
                rnd = (k+lowBound*rnd) % (upperBound - lowBound)
                if rnd < 10 {
                    rnd = rnd + 11
                }
            myArray.append(rnd)
        }
    }
    return myArray
}

func getMirrorArray (array: [Int], sizeOfMatrix: Int = 3) -> [String] {
    let n = sizeOfMatrix
    var mirrorArray = Array(repeating:"  ", count: n*n)
    var index = 1
    var k = 1
    var range = sizeOfMatrix
    var iteration = 0
        for j in array {
            if iteration == range {
                k += 1
                index = k
                iteration = 0
                range -= 1
            }
        mirrorArray[(n*n-index)] = String(j)
        index += n
        iteration += 1
        }
    return mirrorArray
    }
    
func printMirrorArray (array: [String]) -> () {
var x = 0
let sizeOfMatrix = Int(sqrt(Double(array.count)))
    for _ in 0..<sizeOfMatrix {
        print(" ")
            for _ in 0..<sizeOfMatrix {
                print (array[x], terminator: " ")
                x += 1
            }
    }
}

func getRandomSize () -> Int {
    var z = Int(NSDate().timeIntervalSinceReferenceDate * 1000000)
    z = 3+(z % 10)
        if z > 10 {
            z = z - 2
        }
    return z
}

func printResult () -> () {
    let sizeOfMatrix = getRandomSize()
    print ("Size = \(sizeOfMatrix)")
    let countArray = getRndNumbersArray(sizeOfMatrix: sizeOfMatrix)
    printArray(array: countArray, sizeOfMatrix: sizeOfMatrix)
    let mirrorArray = getMirrorArray(array:countArray,sizeOfMatrix:sizeOfMatrix)
    printMirrorArray(array:mirrorArray)
    }

printResult()