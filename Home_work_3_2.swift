typealias weight = Float
typealias height = Float
typealias sex = String

func switchBMI (BMI: weight, sex: sex) -> (sex, weight, sex) {
var tuple = (sex, BMI , " ")
let newSex = sex.uppercased()     
    if newSex == "MALE" {
                switch BMI {
                    case 0...16: tuple = (newSex, BMI , "Underweight")
                    case 16...18.5: tuple = (newSex, BMI , "Deficien body weight")
                    case 18.5...24.99: tuple = (newSex, BMI , "Normal weight")
                    case 25...30: tuple = (newSex, BMI , "Overweight")
                    default: tuple = (newSex, BMI , "Obesity")
                }
            } else 
    if newSex == "FEMALE" {
                switch BMI {
                    case 0...15: tuple = (newSex, BMI , "Underweight")
                    case 15...17.5:tuple = (newSex, BMI , "Deficien body weight")
                    case 17.5...23.99: tuple = (newSex, BMI , "Normal weight")
                    case 24...29: tuple = (newSex, BMI , "Overweight")
                    default: tuple = (newSex, BMI , "Obesity")
                }
            }
    return tuple
} 

func getBMI ( _ weight: weight = 95, _ height: height = 1.81 , _ sex: sex = "Male") -> (sex, weight, sex) {
    var result = (sex, height, sex)
    if (weight > 0 && weight < 400) && (height > 0 && height < 3) && (sex.uppercased() == "MALE" || sex.uppercased() == "FEMALE") {
            let BMI = (weight/(height*height))
            result = switchBMI(BMI: BMI, sex: sex)     
        } else {
            result = ("wrong", 0.0 ,"wrong")
        }
        return result
    }

let result = getBMI()
print ("Sex is \(result.0), index BMI = \(result.1), \(result.2)")